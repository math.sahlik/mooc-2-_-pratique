# Un premier exemple d'analyse d'une séance

Charles Poulmaire a préparé une séance de cours niveau Terminale NSI, pour introduire les bases de données relationnelles. 

Dans un premier temps, nous pouvons nous approprier cette activité en regardant les documents suivants (documents présentés dans la partie 2.1) :
- [La fiche élève](https://gitlab.com/sebhoa/mooc-2-ressources/-/blob/main/1_Penser-Concevoir-Elaborer/fiche_eleve_decouverte_BDR_charles.pdf?inline=false)
- <a href="https://gitlab.com/sebhoa/mooc-2-ressources/-/blob/main/1_Penser-Concevoir-Elaborer/fiche_prof_decouverte_BDR_charles.md" target="_blank">Sa fiche _prof_ de l'activité</a>

Dans un deuxième temps, accéder à l'[analyse de l'activité par l'auteur](https://gitlab.com/sebhoa/mooc-2-ressources/-/blob/main/2_Mettre-en-oeuvre-Animer/fiche_analyse_decouverte_BDR_charles.pdf?inline=false) explicitant la mise en oeuvre de l'activité en classe. 

On retrouve sur le Forum la <a href="https://mooc-forums.inria.fr/moocnsi/moocnsi/t/analyse-decouverte-des-bases-de-donnees-relationnelles/3335" target="_blank">discussion autour de cette activité</a>. 

