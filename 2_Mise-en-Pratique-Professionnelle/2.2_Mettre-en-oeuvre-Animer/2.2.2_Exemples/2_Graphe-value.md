# Une activité hybride sur la recherche de chemin dans un graphe

Thierry Viéville propose cette activité hybride sur la modélisation et la recherche d'un chemin optimal dans un graphe valué.

La fiche élève est constituée d'une première suite d'activités en ligne sur la modélisation d'un graphe. <br/>
Fiche proposée par David Roche et que l'on peut trouver ici : <a href="https://pixees.fr/informatiquelycee/n_site/nsi_term_structDo_graphe.html" target="_blank">https://pixees.fr/informatiquelycee/n_site/nsi_term_structDo_graphe.html</a>

La deuxième partie est l'activité hybride dont il est question. <br/>
La fiche analyse est donc aussi la fiche élève : <a href="https://gitlab.com/sebhoa/mooc-2-ressources/-/blob/main/2_Mettre-en-oeuvre-Animer/fiche-analyse-nsi-term-graphe-algo.md" target="_blank">accéder à la fiche</a>

Nous pouvons échanger sur cet exemple dans <a href="https://mooc-forums.inria.fr/moocnsi/t/introduction-a-lalgorithme-de-dijkstra/3467" target="_blank">cette discussion dédiée du forum</a>.
